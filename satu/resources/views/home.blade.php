@extends('layouts.master')
@section('title','Home')
@section('content')
<div class="content-wrapper">
        <section class="content-header">
          <h1>
                Home
          </h1>
          <ol class="breadcrumb">
            <li class="active"><i class="fa fa-home"></i> Home</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
              <div class="col-md-12">

                  <div class="box box-primary">
                      <div class="box-header with-border">
                          <h3 class="box-title">Welcome, <b>{{Auth::user()->nama}}</b>!</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            This is a web application for Sarkom Test
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
</div>
@stop
