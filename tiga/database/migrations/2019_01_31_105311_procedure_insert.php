<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProcedureInsert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("DROP PROCEDURE IF EXISTS `insert_produk`");
        DB::unprepared("
        CREATE procedure insert_produk(in inama varchar(200), in istok int, in ijumlah int)
        wholeblock:BEGIN
        declare x INT default 0;
        SET x = 1;

        WHILE x <= ijumlah DO
            Insert into produks (nama,stok) values (inama,istok);
            SET x = x + 1;
        END WHILE;
        
        END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
