<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="_token" content="{{ csrf_token() }}"/>
  <title>@yield('title')</title>
  @include('layouts.css')
</head>
<body class="hold-transition skin-blue sidebar-mini-fixed fixed">
<div class="wrapper">

    @include('layouts.header')
    @include('layouts.sidebar')
    @yield('content')

    @include('layouts.footer')

</div>
<!-- ./wrapper -->
@include('layouts.js')
</body>
</html>
