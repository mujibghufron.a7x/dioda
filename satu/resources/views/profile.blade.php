@extends('layouts.master')
@section('title','Users')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Profile
        </h1>
        <ol class="breadcrumb">
        <li class="active"><i class="fa fa-user"></i> Profile</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
            <div class="box-body box-profile">
                @if ($data->foto != null)
                    <img class="profile-user-img img-responsive img-circle" src="{{Storage::url($data->foto)}}" alt="User profile picture">
                @else
                    <img class="profile-user-img img-responsive img-circle" src="{{asset('template/dist/img/avatar5.png')}}" alt="User profile picture">
                @endif

                <h3 class="profile-username text-center">{{$data->nama}}</h3>

                <p class="text-muted text-center">{{$data->email}}</p>
                
                <a href="{{route('edit')}}" class="btn btn-success btn-block" style="@if($data->id != Auth::id()) display:none @endif"><b>Edit Profile</b></a>
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
                <!-- About Me Box -->
                <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-user margin-r-5"></i> Name</strong>
                    <p class="text-muted">
                    {{$data->nama}}
                    </p>
                    <hr>
                    <strong><i class="fa fa-phone margin-r-5"></i> Phone Number</strong>
                    <p class="text-muted">
                    {{$data->no_telepon}}
                    </p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Address</strong>
                    <p class="text-muted">{{$data->alamat}}</p>
                    <hr>
                    <strong><i class="fa fa-venus-mars"></i> Gender</strong>
                    <p>
                        @if ($data->jenis_kelamin == 'Laki-laki')
                            Male
                        @else
                            Female
                        @endif
                    </p>
                    <hr>
                    <strong><i class="fa fa-calendar margin-r-5"></i> Join Date</strong>
                    <p>{{date('d F Y',strtotime($data->created_at))}}</p>
                </div>
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@stop