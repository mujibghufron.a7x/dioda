@extends('layouts.master')
@section('title','Users')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Profile
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{route('profile',['id'=>Auth::id()])}}"><i class="fa fa-user"></i> Profile</a></li>
        <li class="active">Edit profile</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-md-12">
                <!-- About Me Box -->
                <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{route('update')}}" enctype="multipart/form-data" method="POST">
                    @csrf @method('PUT')
                    <div class="box-body">
                      <div class="form-group">
                        <label for="nama">Name</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{$data->nama}}">
                      </div>
                      <div class="form-group">
                        <label for="alamat">Address</label>
                        <textarea name="alamat" class="form-control"  id="alamat" cols="30" rows="3">{{$data->alamat}}</textarea>
                      </div>
                      <div class="form-group">
                        <label for="no_telepon">Phone Number</label>
                        <input type="number" class="form-control" id="no_telepon" name="no_telepon" value="{{$data->no_telepon}}">
                      </div>
                      <div class="form-group">
                        <label for="jenis_kelamin">Gender</label>
                        <select name="jenis_kelamin" class="form-control" id="jenis_kelamin">
                            @if ($data->jenis_kelamin == 'Laki-laki')
                            <option value="Laki-laki" selected>Male</option>
                            <option value="Perempuan">Female</option>
                            @else
                            <option value="Laki-laki">Male</option>
                            <option value="Perempuan" selected>Female</option>
                            @endif
                        </select>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{$data->email}}">
                        </div>
                        <div class="form-group">
                          <label for="foto">Photo</label>
                          <input type="file" id="foto" name="foto">
                        </div>
                        <div class="form-group">
                            <label for="password">Passsword</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Enter your password to save!">
                            @if (session('alert'))
                            <span style="color:red">{{session('alert')}}</span>
                            @endif
                        </div>
                    </div>
                    <!-- /.box-body -->
      
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@stop