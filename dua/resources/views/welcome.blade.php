<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Trigger</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div>
            <h4>Trigger SQL</h4>
            <h3>List Produk</h3>
            <table>
                <thead>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th>Stok</th>
                </thead>
                <tbody>
                    @foreach ($produk as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->stok}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <h3>List Penjualan Produk</h3>
            <button type="button" id="add">Tambah Penjualan</button>
            <div id="formAdd" hidden>
                <form action="{{route('sell')}}" method="post">
                    @csrf @method('POST')
                    <label for="produk">Pilih Produk</label>
                    <select name="produk" id="produk">
                        @foreach ($produk as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    <label for="jumlah">Jumlah</label>
                    <input type="number" name="jumlah" id="jumlah">
                    <input type="submit" name="submit" id="submit">
                </form>
            </div>
            <table>
                    <thead>
                        <th>No</th>
                        <th>Kode Penjualan</th>
                        <th>Produk</th>
                        <th>Jumlah</th>
                    </thead>
                    <tbody>
                        @foreach ($penjualan as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->kode_penjualan}}</td>
                                <td>{{$item->nama}}</td>
                                <td>{{$item->jumlah}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

        </div>
    </body>
    <script src="js/jquery.min.js"></script>
    <script>
    $(document).ready(function(){
        $("#add").click(function(){
            $("#formAdd").prop("hidden", false)
        })
    })
    </script>
</html>
