@extends('layouts.master')
@section('title','Users')
@section('content')
<div class="content-wrapper">
        <section class="content-header">
          <h1>
                Users
          </h1>
          <ol class="breadcrumb">
            <li class="active"><i class="fa fa-users"></i> Users</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
              <div class="col-md-12">
                  <div class="box box-primary">
                      <div class="box-header with-border">
                          <h3 class="box-title">All Users</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="data-users" class="table table-bordered table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Option</th>
                                </thead>
                                <tbody>
                                    @foreach ($data as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->nama}}</td>
                                            <td>{{$item->email}}</td>
                                            <td>
                                                <a href="{{route('profile',['id'=>$item->id])}}" class="btn btn-block btn-success">View Profile</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
</div>
@stop
@section('additionaljs')
<script>
    $(document).ready(function(){
       $('#data-users').DataTable()
})
</script>
@endsection
