<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function allUsers()
    {
        $data = User::orderby('id','desc')->get();
        return view('users', compact('data'));
    }
    public function profile($id)
    {
        $data = User::where('id',$id)->first();
        return view('profile', compact('data'));
    }
    public function edit()
    {
        $id = Auth::id();
        $data = User::where('id',$id)->first();
        return view('edit', compact('data'));
    }
    public function update(Request $request)
    {
        $id                = Auth::id();
        $getUser           = User::where('id', $id)->first();
        $checkPassword     = Hash::check($request->password, $getUser->password);
        if ($checkPassword) {
            if ($request->hasFile('foto')) {
                Storage::delete($getUser->foto);
                $foto           = $request->foto;
                $GetExtension   = $foto->getClientOriginalExtension();
                $path           = $foto->storeAs('public/images', $getUser->nama . '.' . $GetExtension);
                $update         = User::where('id', $id)->update([
                    'nama'          => $request->nama,
                    'email'         => $request->email,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'no_telepon'    => $request->no_telepon,
                    'alamat'        => $request->alamat,
                    'foto'          => $path,
                ]);
            } else {
                $update = User::where('id', $id)->update([
                    'nama'          => $request->nama,
                    'email'         => $request->email,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'no_telepon'    => $request->no_telepon,
                    'alamat'        => $request->alamat,
                    ]);
            }
            return redirect()->route('profile', ['id' => $id]);
        } else {
            return redirect()->back()->with('alert', 'Wrong password! Please try again!');
        }
        
    }
}
