<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MyController extends Controller
{
    public function index()
    {
        $data = DB::select('call select_produk()');
        return view('welcome', compact('data'));
    }

    public function add(Request $request)
    {
        $produk = $request->produk;
        $stok = $request->stok;
        $jumlahLoop = $request->jumlah;
        DB::select('call insert_produk(?,?,?)',[$produk,$stok,$jumlahLoop]);
        return redirect()->back();
    }
}
