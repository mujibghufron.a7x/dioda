<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class MyController extends Controller
{
    public function index()
    {
       $produk = DB::table('produks')->get();
       $penjualan = DB::table('penjualans')
                    ->join('produks','penjualans.id_produk', '=', 'produks.id')
                    ->get();
       return view('welcome', compact('produk','penjualan'));
    }

    public function sell(Request $request)
    {
        $code   = DB::table('penjualans')->count() + 1;
        $insert = DB::table('penjualans')->insert([
            'kode_penjualan' => 'penju-'.$code,
            'id_produk'      => $request->produk,
            'jumlah'         => $request->jumlah,
        ]);
        return redirect()->back();
    }
}
