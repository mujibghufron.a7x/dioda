<?php

use Illuminate\Database\Seeder;
class produk extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<=5 ; $i++) {
            DB::table('produks')->insert([
                'nama' => 'produk'.$i,
                'stok' => 100,
            ]);
        }
    }
}
